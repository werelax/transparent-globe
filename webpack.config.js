var path = require('path');

module.exports = {
  entry: path.join(__dirname, 'src', 'index.js'),
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/'
  },
  resolve : {
    root       : path.join(__dirname, 'src'),
    extensions : ['', '.js', '.jsx', '.json']
  },
  module: {
    loaders: [
      { test: /\.js$/,
        exclude: [/node_modules/],
        loader: 'babel-loader',
        query: {
          presets: ['es2015']
        },
        include: [path.join(__dirname, 'src')]
      },
      { test: /\.json$/,
        exclude: [/node_modules/],
        loader: 'json-loader'
      },
      { test: /\.glsl$/,
        exclude: [/node_modules/],
        loader: 'raw-loader'
      }
    ]
  }
};
