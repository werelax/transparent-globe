// -------------------------
// dependencies
import * as THREE from 'three'
import _ from 'lodash'
import Promise from 'bluebird'
import { setup as setupGraphics } from 'base/graphics'
import { setup as setupGlobe} from 'base/globe'

// -------------------------
// config

// -------------------------
// setup

class Globe {

  constructor (container) {
    try {
      // -------------------------
      // setup
      this.container = container
      this.graphics = setupGraphics(container)
      this.globe = setupGlobe(this.graphics.camera.position)
      this.graphics.scene.add(this.globe.mesh)
      this.graphics.scene.add(this.globe.haloMesh)

      // -------------------------
      // lighting
      this.ambientLight = new THREE.AmbientLight(0x888888, 0.5)
      this.directionalLight = new THREE.DirectionalLight(0xffffff, 1)
      this.directionalLight.position.set(10, 10, 10)
      this.directionalLight.castShadow = true
      this.directionalLight.shadow.camera.near = 0.01
      this.directionalLight.shadow.camera.far = 15
      this.directionalLight.shadow.camera.fov = 45
      this.directionalLight.shadow.camera.left = -1
      this.directionalLight.shadow.camera.right = 1
      this.directionalLight.shadow.camera.top = 1
      this.directionalLight.shadow.camera.bottom = -1
      this.directionalLight.shadow.bias = 0.001
      this.directionalLight.shadow.darkness = 0.2
      this.directionalLight.shadow.mapSize.width = 1024
      this.directionalLight.shadow.mapSize.height = 1024
      this.graphics.scene.add(this.ambientLight)
      this.graphics.scene.add(this.directionalLight)

      // -------------------------
      // initialization
      this.ready = Promise.delay(100)

      // -------------------------
      // animation
      this.clock = new THREE.Clock()
      this.time = 0
      this.render = this.render.bind(this)

      // -------------------------
      // start the render
      this.render()
    } catch (e) {
      // catch and wait
      this.__failed = true
      this.__error = e
      this.ready = Promise.reject(e)
    }
  }

  // -------------------------
  // main loop!

  render () {
    // avoid huge deltas when the tab goes to the bg
    const delta = this.clock.getDelta()
    this.fps = 1 / delta
    this.__lastRender = Date.now()
    this.time += Math.min(0.05, delta)

    if (this.ready.isFulfilled()) {
    }

    this.graphics.render()
    requestAnimationFrame(this.render)
  }

}

window.Globe = Globe
export default Globe
