uniform float time;
uniform float freq;
uniform float width;
uniform float height;
uniform float speed;

void main() {
  // gl_FragColor = vec4(sin(gl_FragCoord.x / 100.0), sin(gl_FragCoord.y / 100.0), 0.0, 1.0);
  float x = gl_FragCoord.x;
  float y = gl_FragCoord.y;

  // float speed = 100.0;
  float f = freq / 256.0;
  float layerDistance = 1.0;
  float waveScale = 15.0;
  float noiseScale = 3.0;
  float pos = x / width;
  float density = 6.0;

  float amplitude = f * waveScale * speed;
  // float modulation = sin(time - pos * 15.0 - f * noiseScale * layerDistance));
  // float modulation1 = sin(time - pos * 15.0 - f * noiseScale * layerDistance);
  float modulation2 = (f * speed * cos(time - x / 50.0));

  layerDistance = 1.0;
  float modulation1 = sin(time - pos * density - f * noiseScale * layerDistance);
  float wave1_y = layerDistance * amplitude * modulation1 + modulation2;

  layerDistance = 0.66;
  modulation1 = sin(time - pos * density - f * noiseScale * layerDistance);
  float wave2_y = layerDistance * amplitude * modulation1 + modulation2;

  layerDistance = 0.33;
  modulation1 = sin(time - pos * density - f * noiseScale * layerDistance);
  float wave3_y = layerDistance * amplitude * modulation1 + modulation2;

  float d1 = abs(y - (wave1_y + height/2.0));
  float d2 = abs(y - (wave2_y + height/2.0));
  float d3 = abs(y - (wave3_y + height/2.0));

  if (d1 < 2.0) {
    gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
  } else if (d2 < 1.5) {
    gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
  } else if (d3 < 1.0) {
    gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
  } else {
    gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
  }
}
