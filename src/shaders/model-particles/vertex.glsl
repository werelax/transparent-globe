precision mediump float;
uniform float time;
uniform float wave_y;
varying vec3 _position;

// -------------------------
// PLAN
// 1. Locate the translation, parameterize x and y (bwlw/abv)
// 2. Locate the rotation, parameterize x, y, z
// 3. Plan easings


// -------------------------
// viewer settings

uniform float escala;
uniform float entry_x;
uniform float entry_rot_x;
uniform float entry_rot_y;
uniform float entry_rot_z;
uniform float exit_x;
uniform float exit_rot_x;
uniform float exit_rot_y;
uniform float exit_rot_z;
uniform float duracion;
uniform float altura;
uniform float fn_rotacion;
uniform float fn_traslacion;
uniform float radio_base_particulas;
uniform float variacion_radio_particulas;
uniform float transparencia_particulas;
uniform float intensidad_particulas;
uniform float distorsion_particulas;
uniform float turbulencia;
uniform float fuerza;

// -------------------------
// utils

#define PI 3.1415926535897932384626433832795
#define HALF_PI 1.5707963267948966

mat4 rotationX( in float angle ) {
	return mat4(	1.0,		0,			0,			0,
                0, 	cos(angle),	-sin(angle),		0,
                0, 	sin(angle),	 cos(angle),		0,
                0, 			0,			  0, 		1);
}

mat4 rotationY( in float angle ) {
	return mat4(	cos(angle),		0,		sin(angle),	0,
                0,		1.0,			 0,	0,
                -sin(angle),	0,		cos(angle),	0,
                0, 		0,				0,	1);
}

mat4 rotationZ( in float angle ) {
	return mat4(	cos(angle),		-sin(angle),	0,	0,
                sin(angle),		cos(angle),		0,	0,
                0,				0,		1,	0,
                0,				0,		0,	1);
}

float rand2(float n) {
  return sin(mod(n * 4328.5453, 360.));
}

float rand(float n) {
  return fract(rand2(n));
}

float rnd(vec2 co){
  return fract(sin(mod(dot(co.xy ,vec2(12.9898,78.233)), 360.)) * 4375.5453);
}

// -------------------------
// accerelation curves

float bezier(float x1, float x2, float x3, float pos) {
  float a1 = mix(x1, x2, pos);
  float a2 = mix(x2, x3, pos);
  return mix(a1, a2, pos);
}


float elasticOut(float t) {
  return sin(-13.0 * (t + 1.0) * HALF_PI) * pow(2.0, -10.0 * t) + 1.0;
}

float backOut(float t) {
  float f = 1.0 - t;
  return 1.0 - (pow(f, 3.0) - f * sin(f * PI));
}

float elasticInOut(float t) {
  return t < 0.5
    ? 0.5 * sin(+13.0 * HALF_PI * 2.0 * t) * pow(2.0, 10.0 * (2.0 * t - 1.0))
    : 0.5 * sin(-13.0 * HALF_PI * ((2.0 * t - 1.0) + 1.0)) * pow(2.0, -10.0 * (2.0 * t - 1.0)) + 1.0;
}

float cubicOut(float t) {
  float f = t - 1.0;
  return f * f * f + 1.0;
}

float sineOut(float t) {
  return sin(t * HALF_PI);
}

float cubicIn(float t) {
  return t * t * t;
}

float cubicInOut(float t) {
  return t < 0.5
    ? 4.0 * t * t * t
    : (t - 1.) * (2. * t - 2.) * (2. * t - 2.) + 1.;
}

float quarticInOut(float t) {
  return t < 0.5
    ? +8.0 * pow(t, 4.0)
    : -8.0 * pow(t - 1.0, 4.0) + 1.0;
}

float tsin(float t) {
  return 1. - sin(HALF_PI + t * HALF_PI);
}

float rsin(float t) {
  return sin(3. * PI * t);
}

// -------------------------
// easing selectors

float txeasing (float t) {
  if (fn_traslacion == 0.) {
    return 0.5;
  }
  if (fn_traslacion == 1.) {
    return t;
  }
  if (fn_traslacion == 2.) {
    return cubicIn(t);
  }
  if (fn_traslacion == 3.) {
    return cubicOut(t);
  }
  if (fn_traslacion == 4.) {
    return cubicInOut(t);
  }
  if (fn_traslacion == 5.) {
    return tsin(t);
  }
  return t;
}

float reasing (float t) {
  if (fn_rotacion == 0.) {
    return 0.5;
  }
  if (fn_rotacion == 1.) {
    return t;
  }
  if (fn_rotacion == 2.) {
    return cubicIn(t);
  }
  if (fn_rotacion == 3.) {
    return cubicOut(t);
  }
  if (fn_rotacion == 4.) {
    return cubicInOut(t);
  }
  if (fn_rotacion == 5.) {
    return rsin(t);
  }
  return t;
}

// -------------------------
// animaiton parameters

#define INTIME 0.5
#define MIDDLETIME 0.5

uniform sampler2D tnoise;
uniform float rtime;

void main() {
  vec3 final;
  // config
  float ftime = time;
  ftime = clamp((ftime * clamp(rand(position.x), .8, 1.)),
                0., ftime);
  float intime = smoothstep(0.0, INTIME, time);
  float mtime = smoothstep(INTIME, MIDDLETIME, time);
  float outtime = smoothstep(MIDDLETIME, 1., time);
  float ttime = smoothstep(0.1, 1., time);

  // -------------------------
  // constants & motion
  vec3 torigin = vec3(entry_x - 50.0, 0., 0.);
  vec3 tend = vec3(exit_x - 50.0, 0., 0.);
  vec3 tvelocity = tend - torigin;

  // -------------------------
  // translation
  vec3 translation = vec3(torigin.x + tvelocity.x * txeasing(ttime),
                          -sin(ttime * PI) * altura + wave_y, // - 7.,
                          0.);

  // -------------------------
  // rotation
  vec3 rorigin = vec3(entry_rot_x, entry_rot_y, entry_rot_z);
  vec3 rend = vec3(exit_rot_x, exit_rot_y, exit_rot_z);
  vec3 rvelocity = rend - rorigin;

  // mat4 rotation =
  mat4 rotation =
    rotationX(rorigin.x + rvelocity.x * reasing(ttime))
    * rotationY(rorigin.y + rvelocity.y * reasing(ttime))
    * rotationX(rorigin.z + rvelocity.z * reasing(ttime));

  // -------------------------
  // model
  vec3 model = translation + (rotation * vec4(position, 1.0)).xyz * escala;

  // -------------------------
  // turbulence
  // vec3 randomVelocity = vec3(rand2(200. * position.y * position.z),
  //                            rand2(30. * position.x / position.z),
  //                            rand2(position.x * position.y));
  // vec3 randomVelocity = vec3(rand2(20. * position.y - cos(position.y * 4321.)),
  //                            rand2(sin(position.x * 1234.) + position.y),
  //                            rand2(position.x * (position.z + 1234.56)));
  // vec3 randomVelocity = texture2D(tnoise,
  //                                 vec2(position.y * 100.5,
  //                                      position.x * 200.)).rgb;

  vec3 randomVelocity = vec3(rand2(20. * position.y - position.x),
                             rand2(2. * position.x + position.y),
                             rand2(position.x * (position.z + 1234.56)));
  vec3 velocity = randomVelocity;
  float nn = turbulencia;

  // velocity.x = ( velocity.x - .5 ) * 1.;
  // velocity.y = ( velocity.y - .5 ) * 1.;
  // velocity.z = ( velocity.z - .5 ) * 1.;

  // -------------------------
  // animation phases

  // final = model;

  if (time < INTIME) {
    // in turbulence
    vec3 newPosition =
      model
      + (velocity * 5.)
      * cubicIn(1. - intime);
    vec3 noise = texture2D(tnoise,
                           vec2(newPosition.x * nn * .015 + rtime * .05,
                                newPosition.y * nn * .02 + rtime * .015)).rgb;
    vec3 noiseVelocity = (noise.rgb - .4) * fuerza;
    // interpolation
    final = newPosition + vec3(noiseVelocity * 5.) * cubicIn(1. - intime);
    // distortion
    final += randomVelocity * distorsion_particulas;
  } else if (time < MIDDLETIME) {
    final = model;
    // final += randomVelocity * distorsion_particulas;
    final += randomVelocity;
  } else {
    // out turbulence
    vec3 newPosition =
      model
      + velocity
      * 2.* outtime;
    vec3 noise = texture2D(tnoise,
                           vec2(newPosition.x * nn * .015 + outtime * .05,
                                newPosition.y * nn * .02 + outtime * .015)).rgb;
    vec3 noiseVelocity = (noise.rgb - .5) * fuerza;
    // interpolation
    final = newPosition + vec3(noiseVelocity * 5.) * cubicIn(outtime);
    // distortion
    final += randomVelocity * distorsion_particulas;
  }

  // final = newPosition;
  _position = position;
  gl_PointSize = (radio_base_particulas + variacion_radio_particulas * rand(position.y));
  gl_Position = projectionMatrix * modelViewMatrix *  modelMatrix * vec4(final, 1.0);

  return;
  }
