precision mediump float;
uniform sampler2D texture;
uniform float alpha;
uniform float color;
uniform float time;
varying vec3 _position;

// -------------------------
// utils

float rnd(vec2 co){
  return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

// -------------------------
// viewer settings

uniform float transparencia_particulas;
uniform float intensidad_particulas;

#define DURATION 1.0
#define SPEED 0.6
#define STAY 4.0

#define M_PI 3.1415926535897932384626433832795

void main() {
  float outtime = smoothstep(0.7, 1., time);
  float intime = smoothstep(0., 0.1, time);
  float a = transparencia_particulas;
  float alpha = intime * (1. - outtime);
  // float alpha = 1.;
  float cvariation = (rnd(_position.xy) - .5) / 2.;
  float c = intensidad_particulas + cvariation;
  vec4 color = vec4(c, c, c, a * alpha);
  vec4 tex = texture2D(texture, gl_PointCoord);
  vec4 final = color * tex;
  gl_FragColor = final;
  }
