attribute float size;
varying vec3 _position;

void main() {
  float x = position[0];
  _position = position;
  // gl_PointSize = size;
  // gl_PointSize = (100.0 - position.z) / 30.0 * 15.0;
  gl_PointSize = 10.0;
  gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}
