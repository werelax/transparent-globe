uniform sampler2D texture;
uniform float freq;
varying vec3 _position;

void main() {
  // vec4 color = vec4(1.0, 1.0, 1.0, min(0.2, max(0.1, freq / 10000.0)));
  float alpha = (7.1 - mod(_position.z, 7.1)) / 300.0;
  float freqAlpha = min(1.2, max(1.0, freq / 80.0));
  vec4 color = vec4(1.0, 1.0, 1.0, alpha * freqAlpha);
  vec4 tex = texture2D(texture, gl_PointCoord);
  vec4 final = color * tex;
  gl_FragColor = final;
  // gl_FragColor = vec4(1.0, 1.0, 1.0, 0.3);
}
