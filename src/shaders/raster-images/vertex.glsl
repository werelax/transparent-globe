precision mediump float;
uniform float time;
varying vec3 _position;

// -------------------------
// viewer settings

uniform float escala;
uniform float rotacion_x;
uniform float rotacion_y;
uniform float rotacion_z;
uniform float radio_base_particulas;
uniform float variacion_radio_particulas;
uniform float turbulencia;
uniform float fuerza;
uniform float distorsion_particulas;

// -------------------------
// utils

#define PI 3.1415926535897932384626433832795
#define HALF_PI 1.5707963267948966

mat4 rotationX( in float angle ) {
	return mat4(	1.0,		0,			0,			0,
                0, 	cos(angle),	-sin(angle),		0,
                0, 	sin(angle),	 cos(angle),		0,
                0, 			0,			  0, 		1);
}

mat4 rotationY( in float angle ) {
	return mat4(	cos(angle),		0,		sin(angle),	0,
                0,		1.0,			 0,	0,
                -sin(angle),	0,		cos(angle),	0,
                0, 		0,				0,	1);
}

mat4 rotationZ( in float angle ) {
	return mat4(	cos(angle),		-sin(angle),	0,	0,
                sin(angle),		cos(angle),		0,	0,
                0,				0,		1,	0,
                0,				0,		0,	1);
}

float rand2(float n) {
  return sin(n * 43758.5453);
}

float rand(float n) {
  return fract(rand2(n));
}

float rnd(vec2 co){
  return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

// -------------------------
// accerelation curves

float bezier(float x1, float x2, float x3, float pos) {
  float a1 = mix(x1, x2, pos);
  float a2 = mix(x2, x3, pos);
  return mix(a1, a2, pos);
}


float elasticOut(float t) {
  return sin(-13.0 * (t + 1.0) * HALF_PI) * pow(2.0, -10.0 * t) + 1.0;
}

float backOut(float t) {
  float f = 1.0 - t;
  return 1.0 - (pow(f, 3.0) - f * sin(f * PI));
}

float elasticInOut(float t) {
  return t < 0.5
    ? 0.5 * sin(+13.0 * HALF_PI * 2.0 * t) * pow(2.0, 10.0 * (2.0 * t - 1.0))
    : 0.5 * sin(-13.0 * HALF_PI * ((2.0 * t - 1.0) + 1.0)) * pow(2.0, -10.0 * (2.0 * t - 1.0)) + 1.0;
}

float cubicOut(float t) {
  float f = t - 1.0;
  return f * f * f + 1.0;
}

float cubicIn(float t) {
  return t * t * t;
}

// -------------------------
// animaiton parameters

#define INTIME 0.3
#define MIDDLETIME 0.7

uniform sampler2D tnoise;
uniform float rtime;

void main() {
  // -------------------------
  // variables
  vec3 final;

  // -------------------------
  // time divisions
  float atime = smoothstep(0.1, 0.9, time);
  float intime = smoothstep(0.0, INTIME, atime);
  float mtime = smoothstep(INTIME, MIDDLETIME, atime);
  float outtime = smoothstep(MIDDLETIME, 1., atime);
  float stime = sin(time * PI);

  // -------------------------
  // model
  mat4 rotation =
    rotationY(rotacion_y)
    * rotationX(rotacion_x)
    * rotationZ(rotacion_z);
  vec3 model =  (rotation * vec4(position, 1.0)).xyz * escala;

  // -------------------------
  // turbulence
  vec3 randomVelocity = vec3(rand2(20. * position.y - position.x),
                             rand2(2. * position.x + position.y),
                             rand2(position.x * (position.z + 1234.56)));
  float nn = turbulencia;

  // -------------------------
  // turbulent movement
  vec3 newPosition;
  vec3 noise;
  vec3 noiseVelocity;

  // -------------------------
  // partitions
  if (atime < INTIME) {
    newPosition = model + (randomVelocity * 10.) * cubicIn(1. - intime);
    noise = texture2D(tnoise,
                           vec2(newPosition.x * nn * .035 + stime * .05,
                                newPosition.y * nn * .04 + stime * .015)).rgb;
    noiseVelocity = (noise.rgb - .5) * fuerza;
    final = newPosition + vec3(noiseVelocity * 5.) * cubicIn(1. - intime);
  } else if (atime < MIDDLETIME) {
    final = model;
  } else {
    newPosition = model + (randomVelocity * 10.) * cubicIn(outtime);
    noise = texture2D(tnoise,
                           vec2(newPosition.x * nn * .035 + stime * .05,
                                newPosition.y * nn * .04 + stime * .015)).rgb;
    noiseVelocity = (noise.rgb - .5) * fuerza;
    final = newPosition + vec3(noiseVelocity * 5.) * cubicIn(outtime);
  }

  _position = position;
  gl_PointSize = (radio_base_particulas + variacion_radio_particulas * rnd(randomVelocity.xy));
  gl_Position = projectionMatrix * modelViewMatrix *  modelMatrix * vec4(final, 1.0);
}
