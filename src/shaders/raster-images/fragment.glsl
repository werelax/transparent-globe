precision mediump float;
uniform sampler2D texture;
uniform float time;
varying vec3 _position;

// -------------------------
// viewer settings

uniform float transparencia_particulas;
uniform float intensidad_particulas;

// -------------------------
// constants

#define PI 3.1415926535897932384626433832795
#define INTIME 0.3
#define MIDDLETIME 0.7

// -------------------------
// utils

void main() {
  // -------------------------
  // time sync
  float atime = smoothstep(0.1, 0.9, time);
  float stime = sin(atime * PI);

  float alpha = stime * transparencia_particulas;
  float c = intensidad_particulas;
  vec4 color = vec4(c, c, c, alpha);
  vec4 tex = texture2D(texture, gl_PointCoord);
  vec4 final = color * tex;
  gl_FragColor = final;
  }
