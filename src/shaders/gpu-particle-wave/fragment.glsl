precision mediump float;

uniform sampler2D texture;
varying vec3 _position;

// -------------------------
// gpu governor uniforms
uniform float freq;
uniform float time;
uniform float color;
uniform float fadeStamp;
// -------------------------
// config
uniform float configAlpha;

#define FADE_DURATION 1.0

void main() {
  // optimized for BIG screens
  // float alpha = (7.1 - mod(_position.z, 7.1)) / 300.0;
  // optimized for small screens
  // float alpha = (7.1 - mod(_position.z, 7.1)) / 100.0;
  // read from config
  float alpha = (7.1 - mod(_position.z, 7.1)) / configAlpha;

  // general
  float freqAlpha = min(1.2, max(1.0, freq / 80.0));

  // fade
  float fadeT = min(1.0, (time - fadeStamp) / FADE_DURATION);
  float fromColor = 1.0 - color;
  float c = mix(fromColor, color, fadeT);
  vec4 particleColor = vec4(c, c, c, alpha * freqAlpha);

  vec4 tex = texture2D(texture, gl_PointCoord);
  vec4 final = particleColor * tex;
  gl_FragColor = final;
  // gl_FragColor = vec4(1.0, 1.0, 1.0, 0.3);
}
