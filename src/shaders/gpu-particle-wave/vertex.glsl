precision mediump float;

varying vec3 _position;

// -------------------------
// gpu governor uniforms
uniform float freq;
uniform float meanFreq;
uniform float time;
uniform float speed;
uniform float minZ;
uniform float maxZ;
// -------------------------
// config
uniform float pointSize;

void main() {
  // governor variables
  float f = max(sqrt(freq) / 100., 0.1);
  float fs = max((meanFreq / 250.), 0.1);
  float ld = position.z;

  // x
  float x = (position.x - 0.5)  * 150.;

  // y
  float pos = position.x;
  float y = 0.;
  y += 3. * sin(pos * 50. * f / 10.);

  // 1
  y += 17. * sin(sqrt(pos) * 10. + time * 0.3);
  y += 20. * sin((1. - pos) * 30. * fs / 10. + time * fs / 10.);
  y += 10. * (1. - pos) * sin(max(f * 10., 4.) * time - pos * 20.);
  y += 5. * pos * sin(2. * time - pos * 10.);

  // 2
  y += 3. * sin((1. - ld) * 10. * f / 10.);
  //movement
  y += 10. * sin(ld * 5. * pos + time * 1.);
  y += 2. * sin(ld * 20. * pos + time * fs);
  y += (1. - pos) * 5. * sin(ld * 10. + time);

  // whole wave amplification
  y *= min(max(fs, 0.001), 5.);

  // z
  float z = minZ + (maxZ - minZ) * position.z;

  // final calculation
  vec3 final = vec3(x, y, z);

  // varying
  _position = final;

  // return values

  // mobile
  // gl_PointSize = 5.0;
  // desktop
  // gl_PointSize = 10.0;
  gl_PointSize = pointSize;

  gl_Position = projectionMatrix * modelViewMatrix * vec4(final, 1.0);
}
