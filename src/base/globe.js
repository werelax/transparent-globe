import * as THREE from 'three'
import vertexShader from 'shaders/globe/vertex.glsl'
import fragmentShader from 'shaders/globe/fragment.glsl'

class Globe {

  constructor (viewVector) {
    this.geometry = new THREE.SphereGeometry(0.5, 32, 32)
    this.material = new THREE.MeshPhongMaterial()
    this.material.shading = THREE.SmoothShading
    this.material.map = new THREE.TextureLoader().load('textures/earthmap1k.jpg')
    this.material.bumpMap = new THREE.TextureLoader().load('textures/earthbump1k.jpg')
    this.material.bumpScale = 0.01
    this.material.specularMap = new THREE.TextureLoader().load('textures/earthspec1k.jpg')
    this.material.specular = new THREE.Color('grey')
    this.material.shininess = 10
    this.mesh = new THREE.Mesh(this.geometry, this.material)
    // -------------------------
    // halo
    this.haloGeometry = new THREE.SphereGeometry(0.502, 32, 32)
    this.haloMaterial = new THREE.ShaderMaterial({
      uniforms: {
        c: { type: 'f', value: 1.0 },
        p: { type: 'f', value: 1.4 },
        glowColor: { type: 'c', value: new THREE.Color(0xffffff) },
        viewVector: { type: 'v3', value: viewVector },
        shading: THREE.SmoothShading
      },
      vertexShader,
      fragmentShader,
      side: THREE.FrontSide,
      blending: THREE.AdditiveBlending,
      transparent: true
    })
    this.haloMesh = new THREE.Mesh(this.haloGeometry, this.haloMaterial)
  }
}

export function setup (viewVector) {
  return new Globe(viewVector)
}
