import _ from 'lodash'
import Promise from 'bluebird'
import * as THREE from 'three'
import vertexShader from 'shaders/model-particles/vertex.glsl'
import fragmentShader from 'shaders/model-particles/fragment.glsl'
import { get } from 'superagent'

require('three-obj-loader')(THREE)

function isVertex (line) {
  // b : true if the line is a vertex
  return _.startsWith(line, 'v ')
}

class ModelLoader {

  constructor () {
    this.model = null
    this.setupTexture()
    this.setupUniforms()
    this.setupMaterial()
  }

  dispose () {
    if (!this.model) { return }
    this.texture.dispose()
    this.noiseTexture.dispose()
    this.model.geometry.dispose()
    this.model.material.dispose()
  }

  // -------------------------
  // init

  setupTexture () {
    const textureLoader = new THREE.TextureLoader()
    this.texture = textureLoader.load('textures/particle2.png')
    this.texture.wrapS = this.texture.wrapT = THREE.RepeatWrapping
    this.noiseTexture = textureLoader.load('textures/perlin-512.png')
    this.noiseTexture.wrapS = this.noiseTexture.wrapT = THREE.RepeatWrapping
  }

  setupUniforms () {
    this.uniforms = {
      pointSize: { value: 3.0 },
      alpha: { value: 0.9 },
      color: { value: 0.6 },
      offset_x: { value: 0.0 },
      offset_y: { value: 0.0 },
      time: { value: 0 },
      rtime: { value: 0 },
      texture: { type: 't', value: this.texture },
      tnoise: { type: 't', value: this.noiseTexture },
      wave_y: { value: 0.0 },

      // -------------------------
      // viewer settings

      escala: { value: 1 },
      // entry
      entry_x: { value: 80 },
      entry_rot_x: { value: 0 },
      entry_rot_y: { value: -1.4 },
      entry_rot_z: { value: 0.5 },
      // exit
      exit_x: { value: 80 },
      exit_rot_x: { value: 0 },
      exit_rot_y: { value: -1.4 },
      exit_rot_z: { value: 0.5 },
      // animation
      duracion: { value: 4 },
      altura: { value: -20 },
      fn_rotacion: { value: 0 },
      fn_traslacion: { value: 0 },
      // shader
      radio_base_particulas: { value: 1 },
      variacion_radio_particulas: { value: 3 },
      transparencia_particulas: { value: 0.6 },
      intensidad_particulas: { value: 0.4 },
      distorsion_particulas: { value: 0.1 },
      // turbulencia
      turbulencia: { value: 0.55 },
      fuerza: { value: 8 }
    }
  }

  setupMaterial () {
    this.material = new THREE.ShaderMaterial({
      uniforms: this.uniforms,
      vertexShader,
      fragmentShader,
      transparent: true,
      // blending: THREE.AdditiveBlending,
      depthWrite: false
    })
  }

  // -------------------------
  // commands

  buildModel () {
    this.model = new THREE.Points(this.geometry, this.material)
  }

  loadGeometry (len) {
    // console.log('> model vertex count:', len)
    this.positions = new Float32Array(len)
    this.geometry = new THREE.BufferGeometry()
    this.geometry.addAttribute(
      'position', new THREE.BufferAttribute(this.positions, 3)
    )
  }

  loadVertices (vertices) {
    for (let i = 0, _len = vertices.length; i < _len; i++) {
      this.positions[i] = vertices[i]
    }
    this.geometry.attributes.position.needsUpdate = true
  }

  normalize () {
    let r
    this.geometry.center()
    this.geometry.computeBoundingSphere()
    r = this.geometry.boundingSphere.radius
    this.geometry.scale(30 / r, 30 / r, 30 / r)
  }

  // -------------------------
  // public interface

  loadText (text) {
    const lines = _.split(text, '\n')
    const vertexLines = _.filter(lines, isVertex)
    const vs = _.flatten(_.map(vertexLines, l => _.split(l, ' ')))
    const vertices = _.map(_.reject(vs, v => v === 'v'), parseFloat)
    this.loadGeometry(vertices.length)
    this.loadVertices(vertices)
    this.normalize()
    this.buildModel()
  }

  loadUrl (url) {
    const req = get(url)
            .then(res => this.loadText(res.text))
    return Promise.cast(req)
  }

  getModel () {
    return this.model
  }

  updateUniforms (values) {
    if (!this.model) { return }
    _.each(values, (v, k) => {
      this.uniforms[k] && (this.uniforms[k].value = v)
    })
  }
}

// -------------------------
// export

export function setup () {
  return new ModelLoader()
}
