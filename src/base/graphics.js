import _ from 'lodash'
import * as THREE from 'three'
import Stats from 'stats.js'
import Detector from 'lib/detector'


export function setup (containerSelector) {
  // initialize basic threejs subsystem

  // -------------------------
  // fail if not WebGL
  if (!Detector.webgl) {
    throw new Error('webgl')
  }

  // DOM element
  const container = document.querySelector(containerSelector)
  const size = { width: window.innerWidth, height: window.innerHeight }

  // renderer
  const renderer = new THREE.WebGLRenderer()
  renderer.setPixelRatio(window.devicePixelRatio || 1)
  // renderer.setPixelRatio(1)
  renderer.setSize(size.width, size.height)
  renderer.setClearColor(0x000000, 0)
  container.appendChild(renderer.domElement)

  // scene + camera
  const scene = new THREE.Scene()
  const camera = new THREE.PerspectiveCamera(45, size.width / size.height)
  camera.position.z = 2
  // camera.position.y = 100
  // camera.position.x = 40
  // camera.lookAt(new THREE.Vector3(1, 0.0, 0.0))

  // -------------------------
  // onresize
  const onResize = () => {
    _.assign(size, { width: window.innerWidth, height: window.innerHeight })
    camera.aspect = size.width / size.height
    camera.updateProjectionMatrix()
    renderer.setSize(size.width, size.height)
    // console.log('> resized')
  }
  window.addEventListener('resize', onResize)

  // stats
  const stats = new Stats()
  container.appendChild(stats.dom)

  // render
  const render = () => {
    stats.update()
    renderer.render(scene, camera)
  }

  return { renderer, scene, camera, stats, render, size }
}
