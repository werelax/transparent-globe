import _ from 'lodash'
import Promise from 'bluebird'
import { Actor } from 'base/actor'
import { setup as setupImageLoader } from 'base/image-loader'

class TextActor extends Actor {

  setupModelLoader () {
    return setupImageLoader()
  }

  load () {
    const options = _.defaults(this.paramsToUniforms(this.json),
                               this.settings)
    this.modelLoader.loadText(this.json.text)
    // console.log('>', options)
    this.updateUniforms(options)
    return Promise.cast(this)
  }
}

// -------------------------
// Entry point

export function setup (settings, options) {
  return new TextActor(settings, null, options)
}
