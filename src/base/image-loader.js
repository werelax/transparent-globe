import _ from 'lodash'
import Promise from 'bluebird'
import * as THREE from 'three'
// import vertexShader from 'shaders/raster-images/vertex.glsl'
// import fragmentShader from 'shaders/raster-images/fragment.glsl'
import vertexShader from 'shaders/model-particles/vertex.glsl'
import fragmentShader from 'shaders/model-particles/fragment.glsl'

// -------------------------
// config

const RASTER_PARTICLE_COUNT = 2000
const RATIO = 0.3

// -------------------------
// aux

function rand (n) {
  return Math.floor(Math.random() * n)
}

function smoothstep (n) {
  return 3 * Math.pow(n, 2) - 2 * Math.pow(n, 3)
}

function easeIn (t) {
  return Math.pow(t, 5)
}

function steepstep (t) {
  // quintic-in-out
  return t < 0.5 ? 16 * Math.pow(t, 5) : 1 + 16 * (--t) * Math.pow(t, 4)
}

// -------------------------
// main class

class ImageLoader {

  constructor () {
    this.model = null
    this.setupGeometry()
    this.setupTexture()
    this.setupUniforms()
    this.setupMaterial()
    this.setupModel()
  }

  dispose () {
    if (!this.model) { return }
    this.texture.dispose()
    this.noiseTexture.dispose()
    this.model.geometry.dispose()
    this.model.material.dispose()
  }

  loadUrl (url) {
    return this
      .loadImageElement(url)
      .then(img => this.setupImgCanvas(img))
      .then(() => this.rasterImageParticles())
  }

  loadText (txt) {
    this.setupTextCanvas(txt)
    this.rasterImageParticles()
  }

  getModel () {
    return this.model
  }

  // -------------------------
  // private methods

  setupTextCanvas (txt) {
    this.canvas = document.createElement('canvas')
    // const m = this.canvasContext.measureText(txt)
    // this.canvas.width = m.width * 10
    // this.canvas.height = 480
    this.canvas.width = 2000
    this.canvas.height = 100
    this.canvasContext = this.canvas.getContext('2d')
    this.canvasContext.font = 'italic 100px Georgia'
    this.canvasContext.textBaseline = 'hanging'
    this.canvasContext.fillStyle = 'rgb(0,0,0)'
    this.canvasContext.fillRect(0, 0, this.canvas.width, this.canvas.height)
    this.canvasContext.fillStyle = 'rgb(255,255,255)'
    this.canvasContext.fillText(txt, 0, 0)
    this.size = {
      width: this.canvasContext.measureText(txt).width,
      height: this.canvas.height
    }
  }

  loadImageElement (url) {
    return new Promise((resolve, reject) => {
      this.img = new Image()
      this.img.onload = () => resolve(this.img)
      this.img.onerror = err => reject(err)
      this.img.src = url
    })
  }

  setupImgCanvas (img) {
    this.canvas = document.createElement('canvas')
    this.canvas.width = img.width
    this.canvas.height = img.height
    this.canvasContext = this.canvas.getContext('2d')
    this.canvasContext.drawImage(img, 0, 0)
    this.size = { width: img.width, height: img.height }
  }

  getImageData () {
    return this.canvasContext.getImageData(0, 0, this.size.width, this.size.height)
  }

  setupModel () {
    this.model = new THREE.Points(this.geometry, this.material)
  }

  setupTexture () {
    const textureLoader = new THREE.TextureLoader()
    this.texture = textureLoader.load('textures/particle2.png')
    this.texture.wrapS = this.texture.wrapT = THREE.RepeatWrapping
    this.noiseTexture = textureLoader.load('textures/perlin-512.png')
    this.noiseTexture.wrapS = this.noiseTexture.wrapT = THREE.RepeatWrapping
  }

  setupUniforms () {
    this.uniforms = {
      pointSize: { value: 3.0 },
      alpha: { value: 0.9 },
      color: { value: 0.6 },
      offset_x: { value: 0.0 },
      offset_y: { value: 0.0 },
      time: { value: 0 },
      rtime: { value: 0 },
      texture: { type: 't', value: this.texture },
      tnoise: { type: 't', value: this.noiseTexture },
      wave_y: { value: 0.0 },

      // -------------------------
      // viewer settings

      escala: { value: 1 },
      // entry
      entry_x: { value: 80 },
      entry_rot_x: { value: 0 },
      entry_rot_y: { value: -1.4 },
      entry_rot_z: { value: 0.5 },
      // exit
      exit_x: { value: 80 },
      exit_rot_x: { value: 0 },
      exit_rot_y: { value: -1.4 },
      exit_rot_z: { value: 0.5 },
      // animation
      duracion: { value: 4 },
      altura: { value: -20 },
      fn_rotacion: { value: 0 },
      fn_traslacion: { value: 0 },
      // shader
      radio_base_particulas: { value: 1 },
      variacion_radio_particulas: { value: 3 },
      transparencia_particulas: { value: 0.6 },
      intensidad_particulas: { value: 0.4 },
      distorsion_particulas: { value: 0.1 },
      // turbulencia
      turbulencia: { value: 0.55 },
      fuerza: { value: 8 }
    }
  }

  setupMaterial () {
    this.material = new THREE.ShaderMaterial({
      uniforms: this.uniforms,
      vertexShader,
      fragmentShader,
      transparent: true,
      depthWrite: false
    })
  }

  setupGeometry () {
    this.positions = new Float32Array(RASTER_PARTICLE_COUNT * 3)
    this.geometry = new THREE.BufferGeometry()
    this.geometry.addAttribute(
      'position', new THREE.BufferAttribute(this.positions, 3)
    )
  }

  imageDataAverage (imageData) {
    const { data, height, width } = imageData
    const len = height * width
    let avg = 0
    for (let i = len; i--;) {
      avg += easeIn((data[i + 0] + data[i + 1] + data[i + 2]) / (255 * 3))
    }
    return avg / len
  }

  sampleImageParticle (imageData) {
    const { data, height, width } = imageData
    // const average = steepstep(imageData.average)
    // const average = easeIn(imageData.average)
    const average = imageData.average
    let watchdog = 0
    while (true) {
      const x = rand(width)
      const y = rand(height)
      const idx = (y * width + x) * 4
      const r = data[idx + 0]
      const g = data[idx + 1]
      const b = data[idx + 2]
      const l = (r + g + b) / (255 * 3)
      const t = easeIn(l)
      if (t > average) return { x, y: height - y }
      if (watchdog++ > RASTER_PARTICLE_COUNT / 10) { return { x: 0, y: 0 } }
    }
  }

  rasterImageParticles () {
    const imageData = this.getImageData()
    imageData.average = this.imageDataAverage(imageData)
    const ratio = this.size.width / this.size.height
    const h = Math.round(Math.sqrt(RASTER_PARTICLE_COUNT / ratio)) * RATIO
    const w = Math.round(h * ratio)
    const s = w / this.size.width
    for (let i = 0, _len = RASTER_PARTICLE_COUNT; i < _len; i++) {
      const idx = i * 3
      const sample = this.sampleImageParticle(imageData)
      this.positions[idx + 0] = (sample.x * s) - (w / 2)
      this.positions[idx + 1] = (sample.y * s) - (h / 2)
      this.positions[idx + 2] = 1
    }
    this.geometry.attributes.position.needsUpdate = true
  }

  updateUniforms (values) {
    if (!this.model) { return }
    _.each(values, (v, k) => {
      this.uniforms[k] && (this.uniforms[k].value = v)
    })
  }

  loadStartingPositions () { return }
}

// -------------------------
// export

export function setup () {
  return new ImageLoader()
}
