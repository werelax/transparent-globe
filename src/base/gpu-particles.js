require('lib/gpu-particles')
import _ from 'lodash'
import config from 'config.js'
import * as THREE from 'three'

class GPUParticles {

  constructor (options) {
    this.particleSystem = new THREE.GPUParticleSystem({
      maxParticles: config.dust.maxParticles
    })
    this.options = options
  }

  setOptions (options) {
    this.options = options
  }

  spawn (x, y, z) {
    this.options.position.x = x
    this.options.position.y = y
    this.options.position.z = z
    this.particleSystem.spawnParticle(this.options)
  }

  update (ticks) {
    this.particleSystem.update(ticks)
  }
}

export function setup (options) {
  return new GPUParticles(options)
}
